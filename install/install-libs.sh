# GIT
sudo yum install -y git 

# DOCKER
sudo yum -y update
sudo yum install -y docker
sudo systemctl enable docker.service
sudo usermod -a -G docker
sudo chmod 666 /var/run/docker.sock
sudo systemctl start docker.service

# MAVEN
#sudo yum install -y maven

# JENKINS
sudo wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat-stable/jenkins.repo
sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
sudo yum upgrade
# Add required dependencies for the jenkins package

# JDK
sudo yum install -y java-11-openjdk
sudo amazon-linux-extras install -y java-openjdk11

# Jenkins
sudo yum install -y jenkins
sudo systemctl daemon-reload
sudo systemctl enable jenkins
sudo systemctl start jenkins
sudo systemctl status jenkins
sudo usermod -a -G jenkins
sudo cat /var/lib/jenkins/secrets/initialAdminPassword




# SOURCE: https://www.jenkins.io/doc/book/installing/linux/#red-hat-centos